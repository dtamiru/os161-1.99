#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#if OPT_A1
#include <mips/trapframe.h>
#include <clock.h>
#endif
#if OPT_A3
#include <vfs.h>
#include <kern/fcntl.h>
#include <limits.h>
#endif

int errno;

// ############# exit() #################

#if OPT_A1
void sys__exit(int exitcode) {
  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  // iterate through children, destroy zombies and detaching the others
  struct array *children = p->p_children;
  if (children) {
    for (int i = children->num - 1; i >= 0; i--) {
      // get and copy child. remove it from array
      struct proc *temp_child = array_get(children, i);
      array_remove(children, i);

      spinlock_acquire(&temp_child->p_lock);
      if (temp_child->p_exitstatus == PROC_EXITED) { // child has exited (zombie) -> destroy
        spinlock_release(&temp_child->p_lock);
        proc_destroy(temp_child);
      }
      else { // child is still running -> detach
        temp_child->p_parent = NULL;
        spinlock_release(&temp_child->p_lock);
      }
    } 
  }

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  spinlock_acquire(&p->p_lock);
  if (p->p_parent == NULL) { // parent has exited!
    spinlock_release(&p->p_lock);
    /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
    proc_destroy(p);
  } else { // parent is still running
    p->p_exitstatus = PROC_EXITED;
    p->p_exitcode = exitcode;
    spinlock_release(&p->p_lock);
  }
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}
#else 
void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}
#endif

// ############# getpid() #################
int
sys_getpid(pid_t *retval)
{
  #if OPT_A1
    *retval = curproc->p_id;
    return(0);
  #else
    /* for now, this is just a stub that always returns a PID of 1 */
    /* you need to fix this to make it work properly */
    *retval = 1;
    return(0);
  #endif
}



// ############# waitpid() #################
#if OPT_A1
int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  struct proc *p = curproc;
  struct proc *temp_child = NULL;

  if (p->p_children) {
    struct array *children = p->p_children;
    int childrenNum = array_num(children);

    for (int i = 0; i < childrenNum; i++) { // find child with p_id pid
      struct proc *child = array_get(children, i);
      if (child->p_id == pid) { // child found!
          temp_child = child;
          array_remove(children, i);
          break;
      }
    }

    if (!temp_child) { // child process with p_id pid not found
      errno = ESRCH;
      return -1;
    }
  }

  // busy polling:
  spinlock_acquire(&temp_child->p_lock); 
  while (!temp_child->p_exitstatus) {
    spinlock_release(&temp_child->p_lock); 
    clocksleep(1); 
    spinlock_acquire(&temp_child->p_lock);
  }
  spinlock_release(&temp_child->p_lock);

  exitstatus = _MKWAIT_EXIT(temp_child->p_exitcode);
  
  proc_destroy(temp_child);

  if (options != 0) {
    return(EINVAL);
  }
 
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}
#else
/* stub handler for waitpid() system call*/
int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}
#endif

// ############# fork() #################
#if OPT_A1
int
sys_fork(pid_t *retval, struct trapframe *tf) 
{
  // Create new process structure for the child process (and assign pid)
  struct proc *child_proc = proc_create_runprogram("child");
  if (!child_proc) { // Error - unable to create proc (this should be forwarded back to the user program)
    errno = ENPROC;
    return -1;
  }
  pid_t child_pid = child_proc->p_id; 
  if (curproc->p_children == NULL) { // if there is no children array, create one
    curproc->p_children = array_create();
    array_init(curproc->p_children);
  }
  
  // Link parent and child procs with pointers
  child_proc->p_parent = curproc;
  unsigned index;
  int insert_retval = array_add(curproc->p_children, child_proc, &index);
  if (insert_retval) { // Error - unable to mallocate
    errno = insert_retval;
    return -1;
  }
  
  // Copy the address space contents from the parent to the child
  int copy_retval = as_copy(curproc_getas(), &(child_proc->p_addrspace));
  if (copy_retval) { // Error - out of memory (this should be forwarded back to the user program)
    errno = ENOMEM;
    return -1;
  }
  
  // create a copy of the parent proc thread's trapframe
  struct trapframe *trapframe_for_child = kmalloc(sizeof(struct trapframe));
  if (!trapframe_for_child) { // Error - unable to mallocate
    errno = ENOMEM;
    return -1;
  }

  *trapframe_for_child = *tf; 
  // create new thread for child process, put the copied tf onto its user stack, and modify it so that fork "returned 0"
  int tfork_retval = thread_fork("child_thread", child_proc, enter_forked_process, trapframe_for_child, 0);
  if (tfork_retval) { // Error - out of memory - operation would block
    errno = ENOMEM;
    return -1;
  }
  
  clocksleep(1);
  *retval = child_pid;
  return 0;
}
#endif // OPT_A1


// ############# execv #################


#if OPT_A3
int sys_execv(char *progname, char **argv) {

  /* handle some input errors */
  if(argv == NULL) return EFAULT;
  if(progname == NULL) return ENOENT;
  if(strlen((char *)progname) > PATH_MAX) return E2BIG;

  // get arg count
  int argc = 0;
  while (argv[argc] != NULL) {
    argc++;
  }
    
  struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

  
  // Copy arguments in from old (forked) user space to the kernel
  char **old_as_args = kmalloc(argc * sizeof(char *));
  if(old_as_args == NULL) return ENOMEM;

  for(int i = 0; i < argc; i++) {
      old_as_args[i] = kmalloc(ARG_MAX);
      if(old_as_args[i] == NULL) return ENOMEM;
  
      size_t copy_length;
      result = copyinstr((const_userptr_t) argv[i], old_as_args[i], ARG_MAX, &copy_length);
      if(result) return result;
  }


	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	struct addrspace *old_as = curproc_setas(as);
	as_activate();
  as_destroy(old_as);


	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}


  // Copy arguments out from kernel space to user space

  vaddr_t *arg_ptrs = (vaddr_t *)kmalloc((argc + 1) * sizeof(vaddr_t));

  for(int i = 0; i < argc; i++) {
    int length = strlen(old_as_args[i]) + 1; 
    size_t copy_length;

    stackptr = stackptr - ROUNDUP(length, 4);

    arg_ptrs[i] = (vaddr_t)stackptr;
    result = copyoutstr(old_as_args[i], (userptr_t)stackptr, length, &copy_length);
    if(result) return result;
  }
  arg_ptrs[argc] = 0;
  
  // Copy out arg_ptrs (argv)
  for(int i = argc; i >= 0; i--) {
      stackptr = stackptr - sizeof(vaddr_t);
      result = copyout(&arg_ptrs[i], (userptr_t)stackptr, sizeof(vaddr_t));
      if(result) return result;
  }

  // free argument arrays
  kfree(arg_ptrs);
  for(int i = 0; i < argc; i++) {
    kfree(old_as_args[i]);
  }
  kfree(old_as_args);
	
  //enter new process(user space)
  //argument painter is the same with stack pointer when user program begin to run
  enter_new_process(argc, (userptr_t)stackptr, stackptr, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}
#endif



